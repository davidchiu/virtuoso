/**
 * Created by davidchiu on 12/16/15.
 */


var VirtuosoClient = (function () {
    var db = new Firebase("https://virtuoso.firebaseio.com/");
    var clientID;

    var getClientName = function() {
        return location.href.split('name=')[1]+'';
    };

    var setPresence = function() {
        var clientName = getClientName();
        var listRef = new Firebase("https://virtuoso.firebaseio.com/presence/");

        var userRef = listRef.push({'name':clientName});
        clientID = userRef.key()
        console.log()

        // Add ourselves to presence list when online.
        var presenceRef = new Firebase("https://virtuoso.firebaseio.com/.info/connected");
        presenceRef.on("value", function(snap) {
            if (snap.val()) {
                userRef.child('connected').set(true);
                // Remove ourselves when we disconnect.
                userRef.onDisconnect().remove();
            }
        });

    }

    var preventIOSsleep = function() {
        var noSleep = new NoSleep();
        noSleep.enable()
    }
    var processMsg = function(message) {
        console.log('processMsg')
        switch(message.type) {
            case 'text':
                $('body').css('background', '#000');
                $('body').html('<div class="message">'+message.message+'</div>')
                break;
            case 'image':
                $('body').css('background', '#000');
                $('body').html('<div class="image"><img src="'+message.message+'"></div>')
                break;
            case 'video':
                $('body').html('<iframe src="'+message.message+'" scrolling="no"></iframe>')
                break;
            case 'app':
                $('body').html('<iframe src="'+message.message+'" scrolling="no"></iframe>')
                break;
        }
    }

    var init = function() {
        preventIOSsleep();
        setPresence();
        db.child("channel").on("value", function(snapshot) {
            var envelope = snapshot.val();

            console.log(envelope)

            if (envelope.client == clientID || envelope.client == 'all')
                processMsg(envelope);
        });

    };

    return {
        init: init
    }
}());

$(document).ready(function() {
    VirtuosoClient.init();
})
