/**
 * Created by davidchiu on 12/17/15.
 */

var VirtuosoController = (function () {
    var db = new Firebase("https://virtuoso.firebaseio.com/");

    var createScreenSelect = function(clients) {
        var options = '';

        $.each(clients, function(index, value) {
            options+='<input type="checkbox" value="'+index+'" name="clients">'+value.name;
        })

        $('.select').html(options)
    }

    var getClients = function() {
        var listRef = new Firebase("https://virtuoso.firebaseio.com/presence/");
        listRef.on("value", function(snap) {
            $('.notice').html("# of online clients = " + snap.numChildren());
            if (snap.numChildren() > 0)
                createScreenSelect(snap.val());
        });
    }

    var sendMessage = function(type) {
        $.each($('input[name="clients"]:checked'), function() {
            switch(type) {
                case 'text':
                    db.child('channel').set({
                        'message': $('textarea').val(),
                        'client': $(this).val(),
                        'type': 'text'
                    });
                    break;

                case 'video':
                    db.child('channel').set({
                        'message': $('a.vid.selected').attr('href'),
                        'client': $(this).val(),
                        'type': 'video'
                    });
                    break;

                case 'image':
                    db.child('channel').set({
                        'message': $('a.img.selected').attr('href'),
                        'client': $(this).val(),
                        'type': 'image'
                    });
                    break;

                case 'app':
                    db.child('channel').set({
                        'message': $('a.app.selected').attr('href'),
                        'client': $(this).val(),
                        'type': 'app'
                    });
                    break;
            }
        })

    }
    var preventIOSsleep = function() {
        var noSleep = new NoSleep();
        noSleep.enable()
    }


    var init = function() {
        preventIOSsleep();
        getClients();

        $('button').click(function() {
            console.log('clicked');
            sendMessage('text');
        });

        $('a.vid').on('click', function(e) {
            e.preventDefault();
            $('a.vid').removeClass('selected');
            $(this).addClass('selected');

            sendMessage('video');
        })

        $('a.img').on('click', function(e) {
            e.preventDefault();
            $('a.img').removeClass('selected');
            $(this).addClass('selected');

            sendMessage('image');
        })

        $('a.app').on('click', function(e) {
            e.preventDefault();
            $('a.app').removeClass('selected');
            $(this).addClass('selected');

            sendMessage('app');
        })

    };

    return {
        init: init
    }
}());

$(document).ready(function() {
    VirtuosoController.init();
});