var hapi = require('hapi');
var server = new hapi.Server();

server.connection({
    port: process.env.PORT || 8000
});

const rootHandler = function (request, reply) {
    reply.view('index');
};

const controllerHandler = function (request, reply) {
    reply.view('controller');
};

const clientHandler = function (request, reply) {
    reply.view('client');
};

server.register(require('inert'), function (err) {

    if (err) {
        throw err;
    }

    server.route({
        method: 'GET',
        path: '/images/{param*}',
        handler: {
            directory: {
                path: './images/'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/js/{param*}',
        handler: {
            directory: {
                path: './js/'
            }
        }
    });
});

server.register(require('vision'), function(err) {

    if (err) {
        console.log("Failed to load vision.");
    }

    server.views({
        engines: { jade: require('jade') },
        isCached: false,
        path: __dirname + '/templates',
        compileOptions: {
            pretty: true
        }
    });

    server.route({ method: 'GET', path: '/', handler: rootHandler });
    server.route({ method: 'GET', path: '/controller', handler: controllerHandler });
    server.route({ method: 'GET', path: '/client', handler: clientHandler });
});

if (!module.parent) {
    server.start(function() {
        console.log('Server started: ' + server.info.uri);
    });
}