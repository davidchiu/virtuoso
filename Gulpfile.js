/**
 * Created by davidchiu on 12/5/15.
 */
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var nodemon = require('gulp-nodemon');

gulp.task('default', ['browser-sync'], function () {
});

gulp.task('browser-sync', ['nodemon'], function() {
    browserSync.init(null, {
        proxy: "http://localhost:8000",
        port: 3000
    });

    gulp.watch("js/*.*").on('change', browserSync.reload);
    gulp.watch("templates/*.*").on('change', browserSync.reload);
    gulp.watch("images/*.*").on('change', browserSync.reload);
});

gulp.task('nodemon', function (cb) {

    var started = false;

    return nodemon({
        script: 'server.js',
        ignore: ['./js/', './templates/']
    }).on('start', function () {
        if (!started) {
            cb();
            started = true;
        }
    });
});